﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        int test_time = 10;
        int point;
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar)== (char)Keys.Enter)
            {
                label2.Visible = true;
                timer1.Start();
                label1.Text = "下注中,時間剩:";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            test_time--;
            label2.Text = test_time.ToString();
            if (test_time == 0)
            {
                label2.Text = "";
                timer1.Stop();
                textBox1.Visible = true;
                textBox1.Focus();
                label1.Text = "請輸入輪盤點數";
                button2.Visible = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            point = Convert.ToInt32(textBox1.Text);
            MessageBox.Show(point.ToString());
        }
    }
}
